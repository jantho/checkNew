package com.xbcx.service.serviceImpl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xbcx.entity.*;
import com.xbcx.exception.CustomerException;
import com.xbcx.mapper.*;
import com.xbcx.model.GroupListVO;
import com.xbcx.model.PageDTO;
import com.xbcx.service.GroupService;
import com.xbcx.util.getDayCha;
import org.aspectj.weaver.ast.Var;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @author 伍炳清
 * @date 2021-10-16 14:50
 */

@Service("GroupService")
public class GroupServiceImpl implements GroupService {

    @Resource
    private GroupMapper groupMapper;
    @Resource
    private UserMapper userMapper;
    @Resource
    private ClassesUserMapper classesUserMapper;
    @Resource
    private ClassesMapper classesMapper;
    @Resource
    private ClassesTimeMapper classesTimeMapper;
    @Resource
    private ClassesActualMapper classesActualMapper;
    @Resource
    private ClassesTimeCopyMapper classesTimeCopyMapper;


    @Override
    public Page<GroupListVO> pageGroup(PageDTO dto) throws CustomerException {
        //判空
        if (dto.getPageNo() == null) {
            throw new CustomerException("当前页数不能为空！");
        }
        if (dto.getPageSize() == null) {
            throw new CustomerException("分页大小不能为空！");
        }
        Page<GroupListVO> page = new Page<>(dto.getPageNo(), dto.getPageSize());
        Page<GroupListVO> groupList = groupMapper.getGroupList(page, dto);
        //加入考勤组人员名集合
        List<GroupListVO> groupListVOs = groupList.getRecords();

        for (GroupListVO group : groupListVOs) {

            LambdaQueryWrapper<Classes> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(Classes::getGroupId, group.getId());
            //得到classes的集合classeslist
            List<Classes> classeslist = classesMapper.selectList(wrapper);

            StringBuilder userNames = new StringBuilder();

            for (Classes classes : classeslist) {
                //单个班次classes
                //根据classes查出userid集合userIdlist
                LambdaQueryWrapper<ClassesUser> wrapper2 = new LambdaQueryWrapper<>();
                wrapper2.eq(ClassesUser::getClassesId, classes.getId());
                List<ClassesUser> userIdlist = classesUserMapper.selectList(wrapper2);
                for (ClassesUser classesUser : userIdlist) {
                    //单个userid
                    User user = userMapper.selectById(classesUser.getUserId());
                    if (userNames.length() == 0) {
                        userNames.append(user.getName());
                    } else {
                        userNames.append("," + user.getName());
                    }
                }
            }
            //添加好了第一个考勤组的人员名单
            group.setAllUserName(userNames.toString());
        }
        //添加好了所有考勤组的人员名单
        groupList.setRecords(groupListVOs);
        return groupList;
    }

    @Override
    public Integer addGroup(Group group) throws CustomerException {

        //判空
        if (group.getBeginTime() == null) {
            throw new CustomerException("起始循环时间不能为空！");
        }
        //根据名字查询考勤组id
        LambdaQueryWrapper<Group> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Group::getName, group.getName())
                .last("limit 1");
        Group newGroup = groupMapper.selectOne(wrapper);
        //判重
        if (newGroup != null) {
            throw new CustomerException("考勤组名称不能重复！");
        }
        group.setIsDel(0);
        groupMapper.insert(group);
        //根据不重复名称查出考勤组id
        Group thisGroup = groupMapper.selectOne(wrapper);

        return thisGroup.getId();


    }

    @Override
    public Integer addClasses(Classes classes) throws CustomerException {
        //判空
        if (classes.getGroupId() == null) {
            throw new CustomerException("考勤组id不能为空！");
        }
        //根据考勤组id和班次序号查出班次id
        LambdaQueryWrapper<Classes> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Classes::getGroupId, classes.getGroupId())
                .eq(Classes::getClassesNum, classes.getClassesNum())
                .last("limit 1");
        Classes newClasses = classesMapper.selectOne(wrapper);
        //判重
        if (newClasses != null) {
            throw new CustomerException("班次" + classes.getGroupId() + "~~第" + classes.getClassesNum() + "班已存在！");
        }
        classes.setIsDel(0);
        classesMapper.insert(classes);

        return classesMapper.selectOne(wrapper).getId();

    }

    @Override
    public void addClassesTime(List<ClassesTime> time) throws CustomerException {

        for (ClassesTime classesTime : time) {
            //判空
            if (classesTime.getClassesId() == null) {
                throw new CustomerException("班次id不能为空！");
            }
            classesTimeMapper.insert(classesTime);
        }

    }

    @Override
    public void addClassesUser(ClassesUser classesUser) throws CustomerException {
        //判空
        Date date = new Date();
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);
        if (classesUser.getClassesId() == null) {
            throw new CustomerException("班次id不能为空！");
        }
        classesUserMapper.insert(classesUser);
        classesActualMapper.delectActualByNowtime(date);
        //        //改变上班时间
//        //根据userid查询actual
//        LambdaQueryWrapper<ClassesActual> wrapper = new LambdaQueryWrapper<>();
//        wrapper.eq(ClassesActual::getUserId, classesUser.getUserId());
//        ClassesActual actual = classesActualMapper.selectOne(wrapper);
//
//        //根据classID查出班次时间集合
//        LambdaQueryWrapper<ClassesTime> wrapper2 = new LambdaQueryWrapper<>();
//        wrapper2.eq(ClassesTime::getClassesId, classesUser.getClassesId());
//        List<ClassesTime> classesTimes = classesTimeMapper.selectList(wrapper2);
//
//        //查出class和group
//        Classes classes = classesMapper.selectById(classesUser.getClassesId());
//        Group group = groupMapper.selectById(classes.getGroupId());
//        actual.setIsClass(classes.getClassesType());
//        actual.setClassNum(classes.getClassesNum());
//        actual.setGroupId(group.getId());
//        actual.setGroupName(group.getName());
//
//        StringBuilder time = new StringBuilder();
//
//        for (ClassesTime timeCopy : classesTimes) {
//            //改变上班时间段
//            if (time.length() == 0) {
//                time.append(timeCopy.getBeginTime().toString().substring(11, 16)
//                        + "~" + timeCopy.getEndTime().toString().substring(11, 16));
//            } else {
//                time.append("," + timeCopy.getBeginTime().toString().substring(11, 16)
//                        + "~" + timeCopy.getEndTime().toString().substring(11, 16));
//            }
//        }
//        actual.setClassTime(time.toString());
//        classesActualMapper.updateById(actual);
    }

    @Override
    public Page<ClassesActual> pageUserClassesList(PageDTO dto, ClassesActual classesActual) {
        Date now = new Date();
        now.setHours(0);
        now.setMinutes(0);
        now.setSeconds(0);
        //默认查询时间为今天
        if (classesActual.getNowTime() == null) {
            classesActual.setNowTime(now);
        }
        //根据时间查询真实排班是否已插入
        LambdaQueryWrapper<ClassesActual> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(ClassesActual::getNowTime, classesActual.getNowTime());

        Page<ClassesActual> page = new Page<>(dto.getPageNo(), dto.getPageSize());
        Page<ClassesActual> classesActualList = classesActualMapper.selectPage(page, wrapper);
        //所有now时间排班
        List<ClassesActual> allNow = classesActualMapper.selectList(wrapper);
        // 查询当天的真实排班
        //1先查出预先设计的考勤组排班
        if (allNow.size() < classesActualMapper.getUserClassesList().size()) {
            //未插入完，先插入
            List<ClassesActual> userClassesList = classesActualMapper.getUserClassesList();
            //循环插入时间段和上班情况
            for (ClassesActual actual : userClassesList) {
                actual.setNowTime(classesActual.getNowTime());
                Group group = groupMapper.selectById(actual.getGroupId());
                //通过userid查出当天班次id进而查出班次
                LambdaQueryWrapper<ClassesUser> userClasswrapper = new LambdaQueryWrapper<>();
                userClasswrapper.eq(ClassesUser::getUserId, actual.getUserId());
                if (classesUserMapper.selectOne(userClasswrapper) != null) {
                    ClassesUser classesUser = classesUserMapper.selectOne(userClasswrapper);
                    Classes classes = classesMapper.selectById(classesUser.getClassesId());
                    //班次数（第几班）参与循环
                    //获取当前循环差
                    int dayNum = getDayCha.differentDays(group.getBeginTime(), actual.getNowTime());
                    //要推进的天数
                    int a = dayNum % group.getClassNum();
                    //现在的班数
                    int b = (a + classes.getClassesNum()) % group.getClassNum();
                    if (b == 0) {
                        b = group.getClassNum();
                    }

                    //现在的班次
                    LambdaQueryWrapper<Classes> Classwrapper = new LambdaQueryWrapper<>();
                    Classwrapper.eq(Classes::getGroupId, group.getId())
                            .eq(Classes::getClassesNum, b);
                    Classes classesNow = classesMapper.selectOne(Classwrapper);
                    //上班情况
                    actual.setClassNum(classesNow.getClassesNum());
                    actual.setIsClass(classesNow.getClassesType());

                    if (classesNow != null) {
                        //上班时间段
                        Integer classesId = classesNow.getId();
                        //现在的班次id为classesId
                        LambdaQueryWrapper<ClassesTime> timewrapper = new LambdaQueryWrapper<>();
                        timewrapper.eq(ClassesTime::getClassesId, classesId);
                        StringBuilder times = new StringBuilder();
                        //班次所有时间段集合
                        List<ClassesTime> classesTimes = classesTimeMapper.selectList(timewrapper);
                        for (ClassesTime classesTime : classesTimes) {
                            if (times.length() == 0) {
                                times.append(classesTime.getBeginTime().toString().substring(11, 16)
                                        + "~" + classesTime.getEndTime().toString().substring(11, 16));
                            } else {
                                times.append("," + classesTime.getBeginTime().toString().substring(11, 16)
                                        + "~" + classesTime.getEndTime().toString().substring(11, 16));
                            }
                        }
                        actual.setClassTime(times.toString());
                    }

                    //现在的班次id为classesId
                    ClassesActual userClassesByTime = classesActualMapper.findActualByUserIdAndTime(actual.getUserId(), actual.getNowTime());
                    if (userClassesByTime == null) {
                        classesActualMapper.insert(actual);
                    }
                } else {
                    //上班情况
                    actual.setIsClass(3);
                    actual.setNowTime(classesActual.getNowTime());
                    ClassesActual userClassesByTime = classesActualMapper.findActualByUserIdAndTime(actual.getUserId(), actual.getNowTime());
                    if (userClassesByTime == null) {
                        classesActualMapper.insert(actual);
                    }

                }
            }
            //根据时间查询真实排班是否已插入
            Page<ClassesActual> page2 = new Page<>(dto.getPageNo(), dto.getPageSize());
            Page<ClassesActual> classesActualList2 = classesActualMapper.selectPage(page2, wrapper);
            return classesActualList2;
        }
        return classesActualList;
    }

    @Override
    public List classesTimeList(Integer actualId) {
        ClassesActual classesActual = classesActualMapper.selectById(actualId);
        Integer classType = classesActual.getClassType();
        Integer groupId = classesActual.getGroupId();
        Integer classNum = classesActual.getClassNum();
        if (classType == 0) {
            System.out.println("班次查询被调用："+groupId+"num:"+classNum);
            //查询班次id
            LambdaQueryWrapper<Classes> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(Classes::getGroupId, groupId)
                    .eq(Classes::getClassesNum, classNum);
            Classes classes = classesMapper.selectOne(wrapper);
            System.out.println("当前班次"+classes);

            LambdaQueryWrapper<ClassesTime> timewrapper = new LambdaQueryWrapper<>();
            timewrapper.eq(ClassesTime::getClassesId, classes.getId());

            List<ClassesTime> classesTimes = classesTimeMapper.selectList(timewrapper);
            System.out.println("班次时间" + classesTimes);
            return classesTimes;
        } else {
            //查询班次id
            LambdaQueryWrapper<ClassesTimeCopy> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(ClassesTimeCopy::getClassesId, actualId);
            List<ClassesTimeCopy> classesTimeCopies = classesTimeCopyMapper.selectList(wrapper);
            return classesTimeCopies;
        }
    }

    @Override
    public void adjustClass(Integer actualId, List<ClassesTimeCopy> classesTimeCopy, Integer isClasses) {
        //isclasses 2:调班 4：调休
        //删除已有copy班次时间
        LambdaQueryWrapper<ClassesTimeCopy> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(ClassesTimeCopy::getClassesId, actualId);
        classesTimeCopyMapper.delete(wrapper);
        ClassesActual classesActual = classesActualMapper.selectById(actualId);
        //调班插入新的班次时间
        if (isClasses == 2) {
            StringBuilder time = new StringBuilder();
            for (ClassesTimeCopy timeCopy : classesTimeCopy) {
                //改变上班时间段
                if (time.length() == 0) {
                    time.append(timeCopy.getBeginTime().toString().substring(11, 16)
                            + "~" + timeCopy.getEndTime().toString().substring(11, 16));
                } else {
                    time.append("," + timeCopy.getBeginTime().toString().substring(11, 16)
                            + "~" + timeCopy.getEndTime().toString().substring(11, 16));
                }
                timeCopy.setClassesId(actualId);
                classesTimeCopyMapper.insert(timeCopy);
            }
            classesActual.setClassTime(time.toString());

        }
        //先改变原本班次的type
        classesActual.setClassType(1);
        classesActual.setIsClass(isClasses);
        classesActualMapper.updateById(classesActual);
    }


}

