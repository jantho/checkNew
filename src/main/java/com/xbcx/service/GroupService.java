package com.xbcx.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xbcx.entity.*;
import com.xbcx.exception.CustomerException;
import com.xbcx.model.*;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author 伍炳清
 * @date 2020-10-16 14:50
 */
public interface GroupService {

    /**
     * 考勤组分页列表
     *
     * @param dto user分页信息
     * @return
     * @throws CustomerException
     */
    Page<GroupListVO> pageGroup(PageDTO dto) throws CustomerException;


    /**
     * 新增考勤组
     *
     * @param group 考勤组
     */

    Integer addGroup(Group group) throws CustomerException;


    /**
     * 新增考勤组班次
     *
     * @param classes 班次
     * @throws CustomerException
     */
    Integer addClasses(Classes classes) throws CustomerException;

    /**
     * 新增班次时间段
     *
     * @param time 时间段集合{map（begintime:"00:00:00";endtime:"00:00:00"）}
     * @throws CustomerException
     */
    void  addClassesTime(List<ClassesTime> time) throws CustomerException;

    /**
     * 新增班次初始化执勤人
     *
     * @param classesUser
     * @throws CustomerException
     */
    void addClassesUser(ClassesUser classesUser) throws CustomerException;
    //    -----------------------------------------------------------------------

    //    -----------------------------------------------------------------------

    //    -----------------------------------------------------------------------

    //    -----------------------------------------------------------------------

    //    -----------------------------------------------------------------------

    /**
     * 实际排班列表
     *
     * @param classesActualVO
     * @return
     */
    Page<ClassesActual> pageUserClassesList(PageDTO page, ClassesActual classesActualVO);

    /**
     * 查询当前班次时间段列表
     * @return
     */
    List  classesTimeList(Integer actualId);

    /**
     * 调整班次时间
     */
    void  adjustClass(Integer actualId,List<ClassesTimeCopy> classesTimeCopy, Integer isClasses);

}
