package com.xbcx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * @Author: 伍炳清
 * @Date: 2021/3/23 11:53
 */
@Data
@NoArgsConstructor
@AllArgsConstructor

@TableName(value = "t_classes_actual")
public class ClassesActual extends Model<ClassesActual> implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 考勤组班次主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 用户id
     */
    @TableField("user_id")
    private Integer userId;
    /**
     * 用户姓名
     */
    @TableField("user_name")
    private String username;
    /**
     * 用户角色
     */
    @TableField("role_name")
    private String roleName;
    /**
     * 用户部门名
     */
    @TableField("department_name")
    private String departmentName;
    /**
     * 考勤组id
     */
    @TableField("group_id")
    private Integer groupId;
    /**
     * 考勤组名称
     */
    @TableField("group_name")
    private String groupName;
    /**
     * 班次时间段
     */
    @TableField("class_time")
    private String classTime;

    /**
     * 上班情况 0：上班 1：休假 2：特殊班次
     */
    @TableField("is_class")
    private Integer isClass;

    /**
     * 班次类型 0 正常班次 1 调班班次
     */
    @TableField("class_type")
    private Integer classType;
    /**
     * 班次数
     */
    @TableField("class_num")
    private Integer classNum;
    /**
     * 要查询班次当天时间
     */
    @TableField("now_time")
    private Date nowTime;


}
