package com.xbcx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * @Author: 伍炳清
 * @Date: 2021/3/23 11:53
 */
@Data
@NoArgsConstructor
@AllArgsConstructor

@TableName(value = "t_classes_time")
public class ClassesTime extends Model<ClassesTime> implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 考勤组班次主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 班次id
     */
    @TableField("classes_id")
    private Integer classesId;

    /**
     * 班开始时间
     */
    @TableField("begin_time")
    private Date beginTime;
    /**
     * 班次结束时间
     */
    @TableField("end_time")
    private Date endTime;


}
