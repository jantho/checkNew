package com.xbcx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * @Author: 伍炳清
 * @Date: 2021/3/23 11:53
 */
@Data
@NoArgsConstructor
@AllArgsConstructor

//定义表名
@TableName(value = "t_groups")
public class Group extends Model<Group> implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 考勤组主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 考勤组名
     */
    @TableField("name")
    private String name;
    /**
     * 考勤组开始时间
     */
    @TableField("begin_time")
    private Date beginTime;
    /**
     * 考勤组班次数
     */
    @TableField("class_num")
    private Integer classNum;

    /**
     * 考勤组是否被删除(0:未删除 1：已删除)
     */
    @TableField("is_del")
    private Integer isDel;


}
