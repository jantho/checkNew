package com.xbcx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * @Author: 伍炳清
 * @Date: 2021/3/23 11:53
 */
@Data
@NoArgsConstructor
@AllArgsConstructor

//定义表名
@TableName(value = "t_classes_user")
public class ClassesUser extends Model<ClassesUser> implements Serializable {

    private static final long serialVersionUID = 15L;
    /**
     * 考勤组用户关联主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 班次id
     */
    @TableField("classes_id")
    private Integer classesId;
    /**
     * 用户id
     */
    @TableField("user_id")
    private Integer userId;


}
