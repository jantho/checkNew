package com.xbcx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * @Author: 伍炳清
 * @Date: 2021/3/23 11:53
 */
@Data
@NoArgsConstructor
@AllArgsConstructor

@TableName(value = "t_classes")
public class Classes extends Model<Classes> implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 考勤组班次主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 考勤组id
     */
    @TableField("group_id")
    private Integer groupId;

    /**
     * 班次数
     */
    @TableField("classes_num")
    private Integer classesNum;
    /**
     * 班次类型
     */
    @TableField("classes_type")
    private Integer classesType;

    /**
     * 班次是否被删除(0:未删除 1：已删除)
     */
    @TableField("is_del")
    private Integer isDel;


}
