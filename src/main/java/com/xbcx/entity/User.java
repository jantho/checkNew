package com.xbcx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;


/**
 * @Author: 伍炳清
 * @Date: 2021/3/23 11:53
 */
@Data
@NoArgsConstructor
@AllArgsConstructor

//定义表名
@TableName(value = "t_user")
public class User extends Model<User> implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 用户主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 用户姓名
     */
    @TableField("name")
    private String name;
    /**
     * 用户账户
     */
    @TableField("account")
    private String account;
    /**
     * 用户密码
     */
    @TableField("pwd")
    private String pwd;
    /**
     * 用户手机号
     */
    @TableField("phone")
    private String phone;
    /**
     * 用户角色id
     */
    @TableField("role_id")
    private Integer roleId;
    /**
     * 用户部门id
     */
    @TableField("dept_id")
    private Integer deptId;
    /**
     * 用户公司id
     */
    @TableField("com_id")
    private Integer comId;
    /**
     * 创建用户的用户id
     */
    @TableField("uid")
    private Integer uid;
    /**
     * 用户创建时间
     */
    @TableField("create_time")
    //Timestamp
    private Date createTime;
    /**
     * 用户更新时间
     */
    @TableField("update_time")
    private Date updateTime;
    /**
     * 用户是否被删除(0:未删除 1：已删除)
     */
    @TableField("is_del")
    private Integer isDel;


}
