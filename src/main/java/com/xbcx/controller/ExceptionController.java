package com.xbcx.controller;

import com.xbcx.exception.CustomerException;
import com.xbcx.util.JsonResult;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartException;

import java.util.Map;

/**
 * @author 伍炳清
 * @date 2020-10-20 11:17
 */
@RestController
@ControllerAdvice
public class ExceptionController {

    JsonResult jsonResult = new JsonResult();

    @ExceptionHandler(MultipartException.class)
    public Map uploadExcepttion(MultipartException e) {

        jsonResult.setCode("0");
        jsonResult.setMsg("文件不能超过1M");
        return jsonResult.getValues();
    }

    @ExceptionHandler(Throwable.class)
    public Map hander(Exception e) {

        if (e instanceof CustomerException) {
            jsonResult.setCode("0");
            jsonResult.setMsg(e.getMessage());

        } else {
            jsonResult.setCode("500");
            jsonResult.setMsg("系统走丢了！");
            e.printStackTrace();
        }

        return jsonResult.getValues();
    }


}
