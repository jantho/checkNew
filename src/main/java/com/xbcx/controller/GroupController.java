package com.xbcx.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xbcx.entity.*;
import com.xbcx.exception.CustomerException;
import com.xbcx.mapper.ClassesMapper;
import com.xbcx.mapper.GroupMapper;
import com.xbcx.mapper.UserMapper;
import com.xbcx.model.GroupListVO;
import com.xbcx.model.PageDTO;
import com.xbcx.model.UserListDTO;
import com.xbcx.model.UserListVO;
import com.xbcx.service.GroupService;
import com.xbcx.service.UserService;
import com.xbcx.util.JsonResult;
import com.xbcx.util.LayuiReplay;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Author: 伍炳清
 * @Date: 2021/3/23 15:52
 */
@RestController
@RequestMapping("/Group")
@CrossOrigin
public class GroupController {
    @Resource
    private GroupService groupService;


    JsonResult jsonResult = new JsonResult();

    @RequestMapping("/pageGroup")
    @CrossOrigin
    public LayuiReplay<GroupListVO> pageGroup(@RequestParam Integer pageNo, @RequestParam Integer pageSize) throws CustomerException {
        PageDTO dto = new PageDTO(pageNo, pageSize);
        Page<GroupListVO> pageGroup = groupService.pageGroup(dto);
        return new LayuiReplay<>(0, "查询成功", (int) pageGroup.getTotal(), pageGroup.getRecords());
    }

    @RequestMapping("/addGroup")
    @CrossOrigin
    public Map<String, Object> addGroup(@RequestBody Group group) throws CustomerException {
        Integer id = groupService.addGroup(group);
        jsonResult.setCode("1");
        jsonResult.setMsg("添加成功");
        jsonResult.setData(id);
        return jsonResult.getValues();
    }

    @RequestMapping("/addClasses")
    @CrossOrigin
    public Map<String, Object> addClasses(@RequestBody Classes classes) throws CustomerException {
        Integer id = groupService.addClasses(classes);
        jsonResult.setCode("1");
        jsonResult.setMsg("添加成功");
        jsonResult.setData(id);
        return jsonResult.getValues();
    }

    @RequestMapping("/addClassesTime")
    @CrossOrigin
    public Map<String, Object> addClassesTime(@RequestBody List<ClassesTime> time) throws CustomerException {
        groupService.addClassesTime(time);
        jsonResult.setCode("1");
        jsonResult.setMsg("添加成功");
        return jsonResult.getValues();
    }

    @RequestMapping("/addClassesUser")
    @CrossOrigin
    public Map<String, Object> addClassesUser(@RequestBody ClassesUser classesUser) throws CustomerException {
        groupService.addClassesUser(classesUser);
        jsonResult.setCode("1");
        jsonResult.setMsg("添加成功");
        return jsonResult.getValues();
    }

    @RequestMapping("/pageUserClassesList")
    @CrossOrigin
    public LayuiReplay<ClassesActual> pageUserClassesList(@RequestParam Integer pageNo, @RequestParam Integer pageSize, @RequestParam(required = false) Date time) throws CustomerException {
        PageDTO dto = new PageDTO(pageNo, pageSize);
        ClassesActual classesActual = new ClassesActual();
        classesActual.setNowTime(time);
        Page<ClassesActual> pageGroup = groupService.pageUserClassesList(dto, classesActual);
        return new LayuiReplay<>(0, "查询成功", (int) pageGroup.getTotal(), pageGroup.getRecords());
    }

    @RequestMapping("/classesTimeList")
    @CrossOrigin
    public Map<String, Object> classesTimeList(@RequestParam Integer id) throws CustomerException {
        List<ClassesTime> classesTimes = groupService.classesTimeList(id);
        jsonResult.setCode("0");
        jsonResult.setMsg("查询成功");
        jsonResult.setData(classesTimes);
        return jsonResult.getValues();
    }

    @RequestMapping("/adjustClass")
    @CrossOrigin
    public Map<String, Object> adjustClass(@RequestParam Integer actualId, @RequestBody List<ClassesTimeCopy> classesTimeCopy, @RequestParam Integer isClasses) throws CustomerException {
        groupService.adjustClass(actualId, classesTimeCopy, isClasses);
        jsonResult.setCode("0");
        jsonResult.setMsg("调班成功");
        return jsonResult.getValues();
    }


}
