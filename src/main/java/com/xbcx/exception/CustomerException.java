package com.xbcx.exception;

/**
 * @author 伍炳清
 * @date 2020/9/22 14:20
 */
public class CustomerException extends Exception {

	private String message;

	public CustomerException(String message) {
		super(message);
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
