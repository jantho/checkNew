package com.xbcx.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * @Author: 伍炳清
 * @Date: 2021/3/23 11:53
 */
@Data
@NoArgsConstructor
@AllArgsConstructor

public class ClassesTimeVO extends Model<ClassesTimeVO> implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 考勤组班次主键
     */
    private Integer id;
    /**
     * 班次id
     */
    private Integer classesId;

    /**
     * 班开始时间
     */
    private Date beginTime;
    /**
     * 班次结束时间
     */
    private Date endTime;


}
