package com.xbcx.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * @Author: 伍炳清
 * @Date: 2021/3/23 11:53
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GroupListVO extends Model<GroupListVO> implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 考勤组主键
     */
    private Integer id;
    /**
     * 考勤组名
     */
    private String name;
    /**
     * 考勤组开始时间
     */
    private Date beginTime;

    /**
     * 考勤组循环班次数
     */
    private Integer classNum;

    /**
     * 考勤组休息天数
     */
    private Integer restNum;
    /**
     * 考勤组人员姓名
     */
    private String allUserName;
    /**
     * 考勤组人数
     */
    private Integer userCount;

    /**
     * 用户是否被删除(0:未删除 1：已删除)
     */
    private Integer isDel;


}
