package com.xbcx.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * @Author: 伍炳清
 * @Date: 2021/3/23 11:53
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserListVO extends Model<UserListVO> implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 用户主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 用户姓名
     */
    private String name;
    /**
     * 用户账户
     */
    private String account;

    /**
     * 用户手机号
     */
    @TableField("phone")
    private String phone;
    /**
     * 用户角色名称
     */
    private String roleName;
    /**
     * 用户部门名称
     */
    private String deptName;
    /**
     * 用户公司名称
     */
    private String comName;
    /**
     * 创建用户的用户姓名
     */
    private String uuN;
    /**
     * 用户创建时间
     */
    //Timestamp
    private Date createTime;
    /**
     * 用户更新时间
     */
    private Date updateTime;
    /**
     * 用户是否被删除(0:未删除 1：已删除)
     */
    private Integer isDel;


}
