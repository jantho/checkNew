package com.xbcx.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @Author: 伍炳清
 * @Date: 2021/3/31 10:06
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserListDTO {

    /**
     * 当前页
     */
    private Integer pageNo;

    /**
     * 条数
     */
    private Integer pageSize;
}
