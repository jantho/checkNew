package com.xbcx.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xbcx.entity.ClassesActual;
import com.xbcx.entity.ClassesTime;

/**
 * @author 伍炳清
 * @date 2020-10-15 20:30
 */
public interface ClassesTimeMapper extends BaseMapper<ClassesTime> {


}
