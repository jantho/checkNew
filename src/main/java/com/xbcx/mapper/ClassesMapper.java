package com.xbcx.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xbcx.entity.Classes;

/**
 * @author 伍炳清
 * @date 2020-10-15 20:30
 */
public interface ClassesMapper extends BaseMapper<Classes> {


}
