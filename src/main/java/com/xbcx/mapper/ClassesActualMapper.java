package com.xbcx.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xbcx.entity.ClassesActual;
import com.xbcx.model.PageDTO;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * @author 伍炳清
 * @date 2020-10-15 20:30
 */
public interface ClassesActualMapper extends BaseMapper<ClassesActual> {

    /**
     * 查询用户真实排班列表
     *
     * @return
     */
    List<ClassesActual> getUserClassesList();

    /**
     * 查询传入时间当日的排班
     *
     * @param nowTime
     * @return
     */
    List<ClassesActual> getUserClassesByTime(@Param("nowTime") Date nowTime);

    /**
     * 查询用户某天的排班（用来查询用户当日是否已有未删除的排班（一般是调班班次不会被删除））
     *
     * @param UserId
     * @param nowTime
     * @return
     */
    ClassesActual findActualByUserIdAndTime(@Param("UserId") Integer UserId, @Param("nowTime") Date nowTime);

    /**
     * 删除某一天以后的非改动排班(修改班次时间后调用可以改变今天级以后的固定排班)
     *
     * @param nowTime
     */
    void delectActualByNowtime(@Param("nowTime") Date nowTime);
}
