//var s=Comm.db('user');
//var u=JSON.parse(s);
//document.getElementById("username").innerText=u.staffName;
//document.getElementById("times").innerText=' '+Comm.dateTimes(new Date());
AJAX.GET('/admin/sysinfo/getTopInfo',function (d) {
    if (d.code==1){
        document.getElementById("users").innerText=d.data.acc;
        document.getElementById('orders').innerText=data.orders;
        document.getElementById('goods').innerText=data.goods;
        document.getElementById('money').innerText=data.sale;
    }
});
function getLineOption(name,data){
    // 指定图表的配置项和数据
    var option = {
        grid: {
            top: '5%',
            right: '1%',
            left: '1%',
            bottom: '10%',
            containLabel: true
        },
        tooltip: {
            trigger: 'axis'
        },
        xAxis: {
            type: 'category',
            data: ['周一','周二','周三','周四','周五','周六','周日']
        },
        yAxis: {
            type: 'value'
        },
        series: [{
            name:name,
            data: data,
            type: 'line',
            smooth: true
        }]
    };
    return option;
}
//注册用户折线图数据
AJAX.GET('/admin/sysinfo/getCountOfReg',function (d) {
    if (d.code==1){
        var da=Comm.GetWeekData(d.data,'reg')
        getLineOption('最新一周新增用户',da);
        var myChart = echarts.init(document.getElementById('main1'));
        myChart.setOption(option);// 使用刚指定的配置项和数据显示图表。
    }
});

AJAX.GET('/admin/sysinfo/getCountOfLogin',function (d) {
    if (d.code==1){
        var da=Comm.GetWeekData(d.data,'acc')
        getLineOption('最新一周活跃用户',da);
        var myChart = echarts.init(document.getElementById('main2'));
        myChart.setOption(option);// 使用刚指定的配置项和数据显示图表。
    }
})

AJAX.GET('/admin/sysinfo/getOrderSaleCount',function (d) {
    if (d.code==1){
        var da=Comm.GetWeekData(d.data,'sale')
        getLineOption('最新一周活跃用户',da);
        var myChart = echarts.init(document.getElementById('main3'));
        myChart.setOption(option);// 使用刚指定的配置项和数据显示图表。
    }
})

AJAX.GET('/admin/sysinfo/getOrderSaleMoney',function (d) {
    if (d.code==1){
        var da=Comm.GetWeekData(d.data,'sales')
        getLineOption('最新一周活跃用户',da);
        var myChart = echarts.init(document.getElementById('main4'));
        myChart.setOption(option);// 使用刚指定的配置项和数据显示图表。
    }
})
AJAX.GET('/admin/sysinfo/orderGoodsType',function (d) {
    var name=[];
    var values=[];
    if (d.code==1){
        var data=d.data;
         for (i in data){
             name[i]=data[i].dictname;
             var js={value:data[i].goods,name:data[i].dictname}
             values.push(js);
         }
         radiusOption(name,values);
        var myChart = echarts.init(document.getElementById('main5'));
        myChart.setOption(option);// 使用刚指定的配置项和数据显示图表。
    }
});

function radiusOption(name,data) {
    // 指定图表的配置项和数据
    var option = {
        tooltip : {
            trigger: 'item',
            formatter: "{a} <br/>{b} : {c} ({d}%)"
        },
        legend: {
            orient: 'vertical',
            left: 'left',
            data: name,
        },
        series : [
            {
                name: '商品类别',
                type: 'pie',
                radius : '55%',
                center: ['50%', '60%'],
                data:data,
                itemStyle: {
                    emphasis: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                }
            }
        ]
    };

}



