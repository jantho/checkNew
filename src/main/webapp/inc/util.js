var Util = {
    checkPrice: function (value, msg) {
        this.checkValue(value, 3, msg);
    },
    checkValue: function (value, type, msg) {
        if (this.checkEmpty(value, msg)) {
            return true;
        }
        var reg1 = /^[1-9]\d*$/; // 正整数
        var reg2 = /^\d+$/; // 非负整数
        var reg3 = /^\+?(\d*\.\d{2})$/; // 两位小数
        var reg4 = /^\d*\.{0,1}\d{0,1}$/; // 一位小数
        if (type == 1) { // 正整数
            if (!reg1.test(value)) {
                Comm.msg("【" + msg + "】只能为正整数", 5);
                return true;
            }
        } else if (type == 2) { // 非负整数
            if (!reg2.test(value)) {
                Comm.msg("【" + msg + "】只能为非负整数", 5);
                return true;
            }
        } else if (type == 3) { // 两位非负小数
            if (!reg2.test(value) && !reg3.test(value) && !reg4.test(value)) {
                Comm.msg("【" + msg + "】只能为非负数，且最多两位小数", 5);
                return true;
            }
        }
    },
    checkEmpty: function (text, msg) {
        var reg = /^[ ]*$/;
        if (typeof (text) == "undefined" || reg.test(text)) {
            Comm.msg("【" + msg + "】不能为空", 5);
            return true;
        }
    },

    subYYYYMMDD: function (d) {
        return d.substring(11);
    },

    categoryLoad: function (pid, selectId, selectedId) {
        var dto = {};
        dto.categoryPid = pid;
        dto.status = 1;
        AJAX.POST("/admin/category/getSimpleCategoryList", dto, function (d) {
            if (d.code == 1) {
                Util.selectLoad(d.data, selectId, "categoryId", "categoryName", selectedId);
            } else {
                Comm.msg(d.msg, 5);
            }
        }, true)
    },

    warehouseLoad: function (selectId, selectedId) {
        AJAX.GET('/admin/warehouse/getWarehouseSimpleList', function (d) {
            if (d.code == 1) {
                Util.selectLoad(d.data, selectId, "warehouseId", "warehouseName", selectedId);
                Comm.form.render();
            } else {
                Comm.msg(d.msg, 5);
            }
        })
    },

    selectLoad: function (data, selectId, value, text, selectedId) {
        var ob = document.getElementById(selectId);
        ob.innerHTML = '<option value="">请选择</option>';
        for (var i in data) {
            var op = document.createElement('option');
            op.setAttribute("value", data[i][value]);
            if (data[i][value] == selectedId) {
                op.setAttribute("selected", "");
            }
            op.innerText = data[i][text];
            ob.appendChild(op);
        }
        Comm.form.render();
    },

    showImgs: function (dataList, imgSize, imgId, divId) {
        if (imgSize == null) {
            imgSize = "max-width: 200px;max-height: 200px;";
        }
        for (var i in dataList) {
            var htmlStr = "<div class=\"layui-inline\">";
            htmlStr += "<img class=\"layui-upload-img\" src=\"" + Comm.OSS.getImgUrl(dataList[i], "s") +
                "\" style=\"" + imgSize + "\" data=\"" + dataList[i] + "\"/><br/>";
            htmlStr +=
                "<button type=\"button\" class=\"layui-btn layui-btn-xs layui-btn-danger\" onclick=\"$(this).parent().remove()\">删除</button></div>";
            $("#" + imgId).val($("#" + imgId).val() + dataList[i] + ",");
            $("#" + divId).append(htmlStr);
        }
    },

    showVideo: function (data, videoId, divId) {
        var htmlStr = "<div class=\"layui-inline\">";
        htmlStr += "<video class=\"layui-upload\" style=\"width: 400px;height: 300px;\" " +
            "src=\"" + Comm.OSS.getImgUrl(data) + "\" controls=\"controls\" data=\"" + data + "\"/><br/>" + "</video>";
        htmlStr +=
            "<button type=\"button\" class=\"layui-btn layui-btn-xs layui-btn-danger\" onclick=\"$(this).parent().remove()\">删除</button></div>";
        $("#" + videoId).val(data);
        $("#" + divId).append(htmlStr);
    },

    showStatus: function (status, type) {
        var stausInfo = [];
        var color = ["green", "red", "gray"];
        var one = ["启用", "禁用", "已关闭"];
        var two = ["上架", "下架", "已关闭"];
        stausInfo.push(one);
        stausInfo.push(two);
        return "<span style='color:" + color[status - 1] + ";'>" + stausInfo[type][status - 1] + "</span>";
    },

    communityLoad: function (stationId, selectedId) {
        AJAX.GET('/admin/community/getCommunitySimpleList?stationId=' + stationId, function (d) {
            if (d.code == 1) {
                Util.selectLoad(d.data, "communityId", "id", "communityName", selectedId);
            } else {
                Comm.msg(d.msg, 5);
            }
        })
    },

    orderListLoad: function (status, warehouseId) {
        Comm.table.render({
            method: 'post',
            contentType: 'application/json',
            elem: '#table',
            url: '/admin/warehouseOrder/getWarehouseOrderList', //列表请求接口
            size: 'sm',
            where: { //接口参数
                warehouseId: warehouseId,
                status: status,
            },
            cols: [
                [{
                    checkbox: true,
                },
                {
                    field: 'orderNo',
                    title: '订单号',
                },
                {
                    field: 'orderType',
                    title: '订单类型',
                    templet: function (d) {
                        return Comm.orderType(d.orderType);
                    },
                },
                {
                    field: 'customerName',
                    title: '收货人',
                },
                {
                    field: 'amount',
                    title: '订单金额',
                    templet: function (d) {
                        return Comm.price(d.amount);
                    },
                },
                {
                    field: '',
                    title: '下单时间',
                    templet: function (d) {
                        return Comm.subYYYY(d.insertTime);
                    },
                },
                {
                    field: '',
                    title: '配送时间',
                    templet: function (d) {
                        if (d.orderType == 2) {
                            return "自提订单";
                        }
                        return Comm.subYYYY(d.expressBeginTime) + ' ~ ' + Comm.subYYYY(d.expressEndTime);
                    },
                    width: 200,
                },
                {
                    field: '',
                    title: '包装',
                    templet: function (d) {
                        return d.deposit > 0 ? "<span style='color:red;'>保温箱</span>" : '塑料袋';
                    },
                },
                {
                    field: 'warehouseName',
                    title: '前置仓',
                },
                {
                    field: 'communityName',
                    title: '小区名称',
                },
                {
                    field: 'status',
                    title: '订单状态',
                    templet: function (d) {
                        return Comm.warehouseOrderStatus(d.status);
                    },
                },
                {
                    field: 'printTime',
                    title: '上次打印时间',
                    hide: status != 30,
                },
                {
                    field: '',
                    title: '操作',
                    toolbar: '#barDemo',
                    width: 180,
                },
                ]
            ],
            done: function (d, curr, count) { },
            limit: 10,
            limits: [10, 20, 30, 40, 50, 60, 70, 80, 90],
            id: 'table',
            page: true,
        });
    },

    warmBagStatus: function (status) {
        switch (status) {
            case 10: return '待回收';
            case 20: return '用户发起回收请求';
            case 30: return '已指派未回收';
            case 40: return '骑手已回收';
            case 50: return '押金已退';
            case 60: return '订单已取消';
            default: return '未知状态';
        }
    },

    deliveryPersonLoad: function (selectId, warehouseId) {
        AJAX.GET('/admin/deliveryPerson/deliveryPersonSimpleList?warehouseId=' + warehouseId, function (d) {
            if (d.code == 1) {
                Util.selectLoad(d.data, selectId, "deliveryPersonId", "name");
            } else {
                Comm.msg(d.msg, 5);
            }
        })
    },

    supplierLoad: function (selectId) {
        AJAX.GET('/admin/supper/simpleList', function (d) {
            if (d.code == 1) {
                Util.selectLoad(d.data, selectId, "mainStockSupperId", "supperName");
            }
        });
    },

}