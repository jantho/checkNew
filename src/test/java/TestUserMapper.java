import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xbcx.MainController;
import com.xbcx.entity.User;
import com.xbcx.exception.CustomerException;
import com.xbcx.mapper.UserMapper;
import com.xbcx.model.UserListDTO;
import com.xbcx.model.UserListVO;
import com.xbcx.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * @Author: 伍炳清
 * @Date: 2021/3/23 10:25
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MainController.class)
public class TestUserMapper {

    @Resource
    private UserMapper userMapper;
    @Resource
    private UserService userService;

    @Test
    public void addUser() {
        Date date = new Date();
        User user = new User(null, "李狗蛋", "lgoudan",
                "123456", "123456", 1, 1, 1,
                1, date, date, 1);
        userMapper.insert(user);

    }

    @Test
    public void page() {
        Page<UserListVO> page = new Page<>(1, 3);
        Page<UserListVO> userList = userMapper.getUserList(page, new UserListDTO());
        System.out.println(userList.getRecords());

    }

    @Test
    public void Test() {
        int createTime = (int) new Date().getTime();
        System.out.println(new Date().getTime());
        System.out.println("转int:" + createTime);
        System.out.println("转回:" + Long.valueOf(createTime));


    }


}
