import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xbcx.MainController;
import com.xbcx.entity.User;
import com.xbcx.mapper.GroupMapper;
import com.xbcx.mapper.UserMapper;
import com.xbcx.model.GroupListVO;
import com.xbcx.model.PageDTO;
import com.xbcx.model.UserListDTO;
import com.xbcx.model.UserListVO;
import com.xbcx.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @Author: 伍炳清
 * @Date: 2021/3/23 10:25
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MainController.class)
public class TestGroupMapper {

    @Resource
    private GroupMapper groupMapper;


    @Test
    public void addGroup() {

    }

    @Test
    public void page() {
        Page<GroupListVO> page = new Page<>(1, 3);
        Page<GroupListVO> userList = groupMapper.getGroupList(page, new PageDTO());
        System.out.println(userList.getRecords());

    }

    @Test
    public void Test() {
        int createTime = (int) new Date().getTime();
        System.out.println("当前时间戳："+new Date().getTime());
        System.out.println("转int:" + createTime);
        System.out.println("转回:" + Long.valueOf(createTime));


    }


}
