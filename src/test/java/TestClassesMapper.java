import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xbcx.MainController;
import com.xbcx.entity.Classes;
import com.xbcx.entity.ClassesTime;
import com.xbcx.entity.User;
import com.xbcx.mapper.ClassesMapper;
import com.xbcx.mapper.ClassesTimeMapper;
import com.xbcx.mapper.UserMapper;
import com.xbcx.model.UserListDTO;
import com.xbcx.model.UserListVO;
import com.xbcx.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @Author: 伍炳清
 * @Date: 2021/3/23 10:25
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MainController.class)
public class TestClassesMapper {

    @Resource
    private ClassesMapper classesMapper;
    @Resource
    private ClassesTimeMapper classesTimeMapper;


    @Test
    public void Test() {
        LambdaQueryWrapper<ClassesTime> timewrapper = new LambdaQueryWrapper<>();
        timewrapper.eq(ClassesTime::getClassesId, 1);
        List<ClassesTime> classesTimes = classesTimeMapper.selectList(timewrapper);
        System.out.println(classesTimes);
    }


}
