import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xbcx.MainController;
import com.xbcx.entity.ClassesActual;
import com.xbcx.entity.User;
import com.xbcx.mapper.ClassesActualMapper;
import com.xbcx.mapper.UserMapper;
import com.xbcx.model.UserListDTO;
import com.xbcx.model.UserListVO;
import com.xbcx.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @Author: 伍炳清
 * @Date: 2021/3/23 10:25
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MainController.class)
public class TestUserClassesMapper {

    @Resource
    private ClassesActualMapper classesActualMapper;

    @Test
    public void getUserClassesByTime() {
        Date date = new Date();
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);
        List<ClassesActual> userClassesByTime = classesActualMapper.getUserClassesByTime(date);
        System.out.println(userClassesByTime);


    }

    @Test
    public void findActualByUserIdAndTime() {
        Date date = new Date();
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);
        ClassesActual userClassesByTime = classesActualMapper.findActualByUserIdAndTime(14,date);
        System.out.println(userClassesByTime);

    }


    @Test
    public void delectActualByNowtime() {
        Date date = new Date();
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);
        classesActualMapper.delectActualByNowtime(date);

    }


}
