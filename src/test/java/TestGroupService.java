import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xbcx.MainController;
import com.xbcx.entity.ClassesActual;
import com.xbcx.entity.ClassesTime;
import com.xbcx.entity.ClassesTimeCopy;
import com.xbcx.exception.CustomerException;
import com.xbcx.mapper.GroupMapper;
import com.xbcx.model.GroupListVO;
import com.xbcx.model.PageDTO;
import com.xbcx.service.GroupService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Author: 伍炳清
 * @Date: 2021/3/23 10:25
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MainController.class)
public class TestGroupService {

    @Resource
    private GroupService groupService;


    @Test
    public void addGroup() {

    }

    @Test
    public void page() {
        PageDTO page = new PageDTO(1, 3);
//        ClassesActual classesActual = new ClassesActual();
//            Page<ClassesActual> groupListVOPage = groupService.pageUserClassesList(page, classesActual);
//            System.out.println(groupListVOPage.getRecords());

        Page<ClassesActual> classesActualPage = groupService.pageUserClassesList(page, new ClassesActual());
        System.out.println(classesActualPage.getRecords());


    }

    @Test
    public void classesTimeList() {
        List<ClassesTime> classesTimes = groupService.classesTimeList(319);
        System.out.println(classesTimes);


    }

    @Test
    public void adjustClass() {
        Date beginTime = new Date();
        beginTime.setHours(0);
        beginTime.setMinutes(0);
        beginTime.setSeconds(0);
        Date endTime = new Date();
        endTime.setHours(17);
        endTime.setMinutes(0);
        endTime.setSeconds(0);

        ClassesTimeCopy classesTimeCopy = new ClassesTimeCopy();
        classesTimeCopy.setBeginTime(beginTime);
        classesTimeCopy.setEndTime(endTime);
        List<ClassesTimeCopy> classesTimeCopies = new ArrayList<>();
        classesTimeCopies.add(classesTimeCopy);
        classesTimeCopies.add(classesTimeCopy);

        groupService.adjustClass(324, classesTimeCopies, 2);

    }

    @Test
    public void Test() {
        int createTime = (int) new Date().getTime();
        System.out.println(new Date().getTime());
        System.out.println("转int:" + createTime);
        System.out.println("转回:" + Long.valueOf(createTime));


    }


}
